I have started implementing the project by setting up the data fetching because it makes sense to have the data available before implementing the UI that will use it.

I usually use AFNetworking for that kind of backend interaction because it simplifies more advanced things but in our setup that is not necessary and I wasn’t supposed to use frameworks.

After making the fetch I would usually add caching or saving the results locally (I usually use Core Data with the MagicalRecord framework for local storage) if needed for future use. Either the images or the data as well. In our scenario this might not be relevant though because I would assume that most searches are one off and only one or two entries from the fetched results would be used because the user is searching for something specific.

Once the data fetch was live I implemented the search bar to allow the user to make the search. I used a simple text field and a button because in most of the apps I have worked on the search bar was always custom and it is much easier to customise a simple UIView with the text field and a button than the provided search bar.

When searching I usually prefer to use blocks rather than the delegate methods because they are much “cleaner” - all the code is in one place. In this case notifications and the delegate methods get the job done.

I have used a UITableViewCell subclass to allow custom cells. The prototype cell is in the Storyboard for easy customisation of the UI. All the config methods are inside the cell code. This reduces the clutter in the ViewController cellForRow method implementation.

The detail view just sets the label texts and fetches the artwork in the background.

There are a number of things that I would add as specified in the comments and TODO bits in the code. Most of the additional things would depend on the required end result, how the app is supposed to work and its intended use so I have not listed any UI/design additions.

