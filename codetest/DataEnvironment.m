//
//  DataEnvironment.m
//  codetest
//
//  Created by Igor Nakonetsnoi on 02/10/2014.
//  Copyright (c) 2014 Istario. All rights reserved.
//

#import "DataEnvironment.h"

@interface DataEnvironment (){
    NSURLConnection *currentConnection;
    NSMutableData *jsonData;
    NSString *currentSearchTerm;
}

@end

@implementation DataEnvironment

@synthesize currentData = _currentData;

// Singleton method to use the same object all accross the app. I am using a separate class to manage all of the data in the app which is a singleton to keep only one instance
+ (DataEnvironment *)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

- (id) init {
    self = [super init];
    if (self) {
        _currentData = [NSArray array];
    }
    return  self;
}

- (void) fetchDataWithSearchTerm:(NSString *)searchTerm {
    
    if (searchTerm.length == 0) {
        // return if the search term is empty
        return;
    }
    currentSearchTerm = searchTerm;
    
    NSArray *searchTerms = [searchTerm componentsSeparatedByString:@" "];
    
    NSMutableString *searchURLString = [NSMutableString stringWithString:@"http://itunes.apple.com/search?term="];
    
    int termsCount = 1;
    for (NSString *searchTermString in searchTerms) {
        [searchURLString appendString:[NSString stringWithFormat:@"%@", searchTermString]];
        if (termsCount != searchTerms.count) {
            // add a "+" to connect the search terms if it's not the last search term
            [searchURLString appendString:@"+"];
        }
        termsCount ++;
    }
    NSLog(@"endurlString %@", searchURLString);
    
    jsonData = [NSMutableData data];
    
    NSURL *searchURL = [NSURL URLWithString:searchURLString];
    
    NSMutableURLRequest *searchRequest = [NSMutableURLRequest requestWithURL:searchURL];
    
    searchRequest.HTTPMethod = @"GET";
    
    [searchRequest setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    if( currentConnection)
    {
        // if there is a connection in progress then cancel it and create a new one further down
        [currentConnection cancel];
        currentConnection = nil;
    }
    
    if( !currentConnection )
    {
        currentConnection = [[NSURLConnection alloc] initWithRequest:searchRequest delegate:self startImmediately:YES];
        
    }
    
}

- (void) saveFetchedData:(NSDictionary *)dataDict withSearchTerm:(NSString *) searchTerm{
    //TODO: we could save the fetched data with the search term locally to provide a cached result in the future if the user is offline or for a quick fetching before the server call returns. We could either save the first batch of results or save all by adding them in as the user scrolls down in the main view. I would probably use Core Data for that since in most apps with a backend there is a need for some sort of local storage anyway.
    
    // for now I just used this to have the keys in the results at hand.
    
//    NSLog(@"saving data %@ with SearchTerm %@", [dataDict allKeys], searchTerm);
    
}


#pragma mark - NSURLConnectionDataDelegate methods
- (void)connection:(NSURLConnection*)connection didReceiveData:(NSData*)data
{
    [jsonData appendData:data];
}
- (void)connection:(NSURLConnection*)connection didFailWithError:(NSError*)error
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error fetching results" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alertView show];
    currentConnection = nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    
    if (error != nil) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error parsing results" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];
    }
    else {
        for (NSDictionary *resultDict in [jsonArray objectForKey:@"results"]) {
            [self saveFetchedData:resultDict withSearchTerm:currentSearchTerm];
        }
        
        _currentData = [jsonArray objectForKey:@"results"];
        
        [[NSNotificationCenter defaultCenter] performSelectorOnMainThread:@selector(postNotification:) withObject:[NSNotification notificationWithName:@"searchResultsUpdated"   object:self  userInfo:nil] waitUntilDone:NO];
    }
    
    currentConnection = nil;
    
}



@end
