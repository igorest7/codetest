//
//  TrackTableViewCell.h
//  codetest
//
//  Created by Igor Nakonetsnoi on 02/10/2014.
//  Copyright (c) 2014 Istario. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

- (void) configureWithTrack:(NSDictionary *) trackDict;

@end
