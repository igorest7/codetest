//
//  DetailViewController.h
//  codetest
//
//  Created by Igor Nakonetsnoi on 02/10/2014.
//  Copyright (c) 2014 Istario. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) NSDictionary *detailItem;
@end

