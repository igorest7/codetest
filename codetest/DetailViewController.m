//
//  DetailViewController.m
//  codetest
//
//  Created by Igor Nakonetsnoi on 02/10/2014.
//  Copyright (c) 2014 Istario. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *artworkIV;
@property (weak, nonatomic) IBOutlet UILabel *trackNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *albumNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *artistNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *releaseDateLabel;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self addRoundedCornersToArtwork];
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Managing the detail item

- (void)setDetailItem:(NSDictionary *)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // TODO: I would add multi row Labels to properly show long text lables if they don't fit on the screen.
    // Update the user interface for the detail item.
    if (self.detailItem) {
        
        if ([self.detailItem objectForKey:@"trackName"]) {
            self.trackNameLabel.text = [NSString stringWithFormat:@"Track name: %@",[self.detailItem objectForKey:@"trackName"]];
        }
        if ([self.detailItem objectForKey:@"collectionName"]) {
            self.albumNameLabel.text = [NSString stringWithFormat:@"Album name: %@",[self.detailItem objectForKey:@"collectionName"]];
        }
        if ([self.detailItem objectForKey:@"artistName"]) {
            self.artistNameLabel.text = [NSString stringWithFormat:@"Artist name: %@",[self.detailItem objectForKey:@"artistName"]];
        }
        if ([self.detailItem objectForKey:@"trackPrice"]) {
            self.priceLabel.text = [NSString stringWithFormat:@"Track price: %@",[NSString stringWithFormat:@"%@ %@",[self.detailItem objectForKey:@"currency"],[self.detailItem objectForKey:@"trackPrice"]]];
        }
        if ([self.detailItem objectForKey:@"artworkUrl100"]) {
            // convert the date from string to NSDate and then trim to the actual date - we don't need to show time here
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
            
            NSDate *releaseDate = [dateFormatter dateFromString:[self.detailItem objectForKey:@"releaseDate"]];
            
            dateFormatter.dateFormat = @"dd-MM-yyyy";
            
            self.releaseDateLabel.text = [NSString stringWithFormat:@"Release date: %@",[dateFormatter stringFromDate:releaseDate]];
            
            NSURL *imageURL = [NSURL URLWithString:[self.detailItem objectForKey:@"artworkUrl100"]];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                // load the images in the background
                NSData *tempData = [NSData dataWithContentsOfURL:imageURL];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    // update the UI on the main thread.
                    self.artworkIV.image = [UIImage imageWithData:tempData];
                });
            });
        }
        
    }
}

- (void) addRoundedCornersToArtwork {
    self.artworkIV.layer.cornerRadius = 50.0;
    self.artworkIV.layer.masksToBounds = YES;
}

@end
