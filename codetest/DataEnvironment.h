//
//  DataEnvironment.h
//  codetest
//
//  Created by Igor Nakonetsnoi on 02/10/2014.
//  Copyright (c) 2014 Istario. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DATAENV [DataEnvironment sharedInstance]

@interface DataEnvironment : NSObject <NSURLConnectionDataDelegate>

@property (nonatomic, readonly) NSArray *currentData;

+ (DataEnvironment *)sharedInstance; // singleton method

- (void) fetchDataWithSearchTerm:(NSString *)searchTerm;

@end
