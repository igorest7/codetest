//
//  MasterViewController.m
//  codetest
//
//  Created by Igor Nakonetsnoi on 02/10/2014.
//  Copyright (c) 2014 Istario. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "TrackTableViewCell.h"

@interface MasterViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *searchTF;

@property NSArray *objects;
@end

@implementation MasterViewController

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableView) name:@"searchResultsUpdated" object:nil];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"searchResultsUpdated" object:nil];
}

-(void) reloadTableView {
    // save the data from the DataEnvironment to this class because if a new search is initiated and the user scrolls the tableView the DataEnvironment data will reset (during the search process) which could cause problems when configuring cells.
    self.objects = DATAENV.currentData;
    [self.tableView reloadData];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDictionary *object = self.objects[indexPath.row];
        [[segue destinationViewController] setDetailItem:object];
    }
}

#pragma mark - IBActions
- (IBAction)searchPressed:(id)sender {
    // I am not using an Apple search bar component because in most apps the search bar has to be customised in some way and it is much easier and quicker to do it using a simple textField and a button.
    if (self.searchTF.text.length == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No search term" message:@"Please enter a search term in the field provided" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];
    }else{
        [self.searchTF resignFirstResponder]; // hide the keyboard because the user is now interested in search results
        [DATAENV fetchDataWithSearchTerm:self.searchTF.text];
    }
}

#pragma mark - Table View

// TODO: I would add variable row height to show more text if required (at the moment the app follows iTunes layout where text gets cut off.
// TODO: I would add offset to the fetching call and then fetch the next 50 results when the user scrolls down to the bottom of the table view. At the moment the table view is limited to 50 results that come back from the backend by default.

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TrackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"trackCell" forIndexPath:indexPath];

    NSDictionary *trackObject = self.objects[indexPath.row];
    [cell configureWithTrack:trackObject];
    return cell;
}

@end
