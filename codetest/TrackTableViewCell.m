//
//  TrackTableViewCell.m
//  codetest
//
//  Created by Igor Nakonetsnoi on 02/10/2014.
//  Copyright (c) 2014 Istario. All rights reserved.
//

#import "TrackTableViewCell.h"

@implementation TrackTableViewCell

@synthesize imageView = _imageView;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) configureWithTrack:(NSDictionary *) trackDict {
    
    [self.titleLabel setText:[trackDict objectForKey:@"trackName"]];
    [self.subtitleLabel setText:[trackDict objectForKey:@"artistName"]];
    [self loadImageWithURL:[trackDict objectForKey:@"artworkUrl60"]];
}

- (void) loadImageWithURL:(NSString *) urlString {
        NSURL *imageURL = [NSURL URLWithString:urlString];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        // load the images in the background
        NSData *tempData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // update the UI on the main thread.
            self.imageView.image = [UIImage imageWithData:tempData];
            [self setNeedsLayout];
        });
    });
}

- (void)prepareForReuse {
    // set the image to nil so that it wouldn't be visible when the cell is reused.
    self.imageView.image = nil;
}

@end
